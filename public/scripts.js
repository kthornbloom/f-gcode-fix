window.addEventListener('load', function () {
  
const processBtn = document.getElementById('process-btn'),
      messaging = document.getElementById('messaging'),
      copy = document.getElementById('copy'),
      loader = document.getElementById('loader-wrap');
var filteredArray = '';

// Handle Input
const originalTextarea = document.getElementById('usergcode'),
      modifiedTextarea = document.getElementById('modifiedgcode');

originalTextarea.addEventListener('input', function(){
  processBtn.removeAttribute('disabled');
  messaging.innerHTML = '';
})

demo.addEventListener('click', function(e){
  e.preventDefault();
  originalTextarea.value = demoGcode;
  processBtn.removeAttribute('disabled');
})

// Submit Form
processBtn.addEventListener('click', function(e){
	e.preventDefault();
  messaging.innerHTML = '';
  loader.style.display = 'flex';
  const opt1 = document.getElementById('restorerapids'),
        opt2 = document.getElementById('homing'),
        //opt3 = document.getElementById('endhoming'),
        opt4 = document.getElementById('zfirst'),
        opt5 = parseInt(document.getElementById('clearanceHeight').value),
        opt6 = parseInt(document.getElementById('retractHeight').value),
        zMax = opt5 + opt6,
  			original = originalTextarea.value;
  
  if (!original){
    messaging.innerHTML += '<div class="error">Forget something?</div>';
    loader.style.display = 'none';
    return;
  }
	var originalAsArray = original.split(/\r?\n/),
  		restoreRapids = false,
      removeHoming = false,
      //removeEndhome = false,
      moveZfirst = false;
  
  // check settings
  if(opt1.checked){restoreRapids = true}
  if(opt2.checked){removeHoming = true}
  // if(opt3.checked){removeEndhome = true}
  if(opt4.checked){moveZfirst = true}
        
  if(removeHoming){
    // kill first   G28 G91 Z0
    if(originalAsArray.includes('G28 G91 Z0')){
      filteredArray = originalAsArray.filter(e => e !== 'G28 G91 Z0');
    } else {
      filteredArray = originalAsArray;
      messaging.innerHTML += '<div class="error">Initial homing not removed. ("G28 G91 Z0" not found)</div>'
    }
  } else {
    filteredArray = originalAsArray;
  }
  
  if(removeHoming){
  	// kill everything after the last lift to Z15 or whatev
    if(originalAsArray.includes('Z'+zMax)){
      let endHomeindex = filteredArray.findLastIndex((element) => element == 'Z'+zMax);
      if(endHomeindex && zMax){
        filteredArray.splice(endHomeindex + 1);
      } else {
        messaging.innerHTML += '<div class="error">End homing not removed. (check heights)</div>'
      }
    } else {
      messaging.innerHTML += '<div class="error">End homing not removed. (check heights)</div>'
    }
  }
  
  if(moveZfirst){
  	// Move the first Z15 to before the first X/Y move
    let firstZ = filteredArray.findIndex((element) => element == 'Z'+zMax);  
    if(firstZ && firstZ < 20){
    	filteredArray[firstZ] = 'G0 '+filteredArray[firstZ];
      var element = filteredArray[firstZ];
      filteredArray.splice(firstZ, 1);
      filteredArray.splice((parseInt(firstZ - 1)), 0, element); 
    } else {
    	messaging.innerHTML += '<div class="error">Z First not completed. (No initial Z movement found within first 20 lines.)</div>'
    }
  }

  var rapidsRestored = 0;
  if(restoreRapids){    
    for (var i = 0; i < filteredArray.length; i++) {
        if (filteredArray[i] == 'Z'+opt6) {
        	filteredArray[i] = 'G0\nZ'+opt6, 
          filteredArray[i + 1] = filteredArray[i + 1]+'\nG1';
          rapidsRestored++;
        }
    }
  }

  // Add line breaks
  filteredArray = filteredArray.join('\n');     
  
  // print out the result
  modifiedTextarea.value = filteredArray

  originalTextarea.scrollTop = -50;
  modifiedTextarea.scrollTop = -50;
  

  // Copy to Clipboard
  navigator.clipboard.writeText(filteredArray).then(function() {
    messaging.innerHTML += '<div class="success"><b>Done!</b></div>';
    if(restoreRapids && rapidsRestored > 0){
      messaging.innerHTML += '<div class="success">'+rapidsRestored+' rapids restored.</div>';
    } else if (restoreRapids) {
      messaging.innerHTML += '<div class="warn">No rapids found to restore. Are your retract and clearance heights correct?</div>'
    }
  }, function(err) {
    console.error('Could not copy to clipboard: ', err);
  });  

  setTimeout(function(){
    loader.style.display = 'none';
    copy.style.display = 'block';
    messaging.innerHTML += '<div class="warn">Always <a href="https://ncviewer.com/" target="_blank">review modified gcode</a> before use.</div>';
  }, 500);

});

// Copy btn
copy.addEventListener('click', function(e){
  e.preventDefault();
  navigator.clipboard.writeText(filteredArray).then(function() {
    messaging.innerHTML = '<div class="success"><b>Neat!</b> Code copied to clipboard.</div>';
  }, function(err) {
    console.error('Could not copy to clipboard: ', err);
  });  
})

// Scroll divs
function select_scroll_1(e) { modifiedTextarea.scrollTop = originalTextarea.scrollTop; }
function select_scroll_2(e) { originalTextarea.scrollTop = modifiedTextarea.scrollTop; }

originalTextarea.addEventListener('scroll', select_scroll_1, false);
modifiedTextarea.addEventListener('scroll', select_scroll_2, false);

})
