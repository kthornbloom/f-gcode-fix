https://kthornbloom.gitlab.io/f-gcode-fix/

## Who this is for

 - homemade / DIY / hobbiest CNCs
 - CNCs running on GRBL
 - Folks using single operation code exported from Fusion 360

## What this can (optionally) do:
 - Restores rapids that may have been lost for mysterious reasons
 - Removes start and end homing. For machines without limit switches, or for people who just like to set the bit in a certain spot and go. 
 - Ensures that the Z axis moves before the X & Y so it doesn't drag on your workpiece
